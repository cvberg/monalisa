
#include <TFMini.hpp>

#define PY_VERSION_HEX 0x02700000
#include <boost/python.hpp>

BOOST_PYTHON_MODULE(TFMiniPy)
{
  using namespace boost::python;
  class_<TFMini>("TFMini", init<std::string>())
    .def("takeNewMeasurement", &TFMini::takeNewMeasurement)
    .def("getDistance", &TFMini::getDistance)
    .def("getStrength", &TFMini::getStrength);
}
