// Defines
#define TFMINI_BAUDRATE   115200

// Timeouts
#define TFMINI_MAXBYTESBEFOREHEADER       30
#define TFMINI_MAX_MEASUREMENT_ATTEMPTS   10

// States
#define ERROR_SERIAL_NOHEADER             1
#define ERROR_SERIAL_BADCHECKSUM          2
#define ERROR_SERIAL_TOOMANYTRIES         3
#define ERROR_SERIAL_READ                 4
#define ERROR_SERIAL_NOFRAME              5
#define MEASUREMENT_OK                    10

#include <TFMini.hpp>
#include <termios.h> // For tcflush
#include <unistd.h>
#include <array>
#include <algorithm> // for std::search

// Constructor
TFMini::TFMini(std::string device_name):
  TOFSensor(device_name, TFMINI_BAUDRATE)
{
  // Set standard output mode
  setStandardOutputMode();
}

// Set the TF Mini into the correct measurement mode
void TFMini::setStandardOutputMode() {
  // Set to "standard" output mode (this is found in the debug documents)
  std::array<uint8_t, 8> mode_stdout = {0x42, 0x57, 0x02, 0x00, 0x00, 0x00, 0x01, 0x06};
  write(port, mode_stdout.data(), mode_stdout.size());
}

uint16_t TFMini::getDistance() {
  return distance;
}

uint16_t TFMini::getStrength() {
  return strength;
}

void TFMini::takeNewMeasurement() {
  // Flush serial port buffer to avoid reading old data
  tcflush(port,TCIOFLUSH);
  bool success = false;
  int max = 10;
  std::vector<uint8_t> frame;
  for(int tries = 0; not success and tries < max; ++tries){
    // Grab a frame
    frame = grabFrame(); 
    if(state==MEASUREMENT_OK){
      // Calculate checksum (sum of all bytes except the last one)
      int sum = 0;
      for(int i = 0; i < frame.size() - 1; i++)
        sum += frame[i];
    
      // The final checksum in given on one byte. Use a bitmask to "crop" the calculated checksum 
      sum &= (0x000000FF); 
      if(sum == frame.back())
        success = true;
      else
        state==ERROR_SERIAL_BADCHECKSUM;
    }
  }
  // throw the last error if we could not correctly read a new frame
  if(not success){
    switch(state){
      case(ERROR_SERIAL_NOHEADER):
        throw std::runtime_error{"Could not find frame header"};
      case(ERROR_SERIAL_BADCHECKSUM):
        throw std::runtime_error{"Bad checksum"};
      case(ERROR_SERIAL_READ):
        throw std::runtime_error{"Error while reading from serial port"};
      case(ERROR_SERIAL_NOFRAME):
        throw std::runtime_error{"Could not read rest of frame"};
    }
  }

  // Distance is given by 2nd and 3rd byte. 
  // 2nd byte is low byte
  // 3rd byte is high byte
  distance = (frame[3] << 8) + frame[2];

  // Strength is given by 4th and 5th byte. 
  // 4th byte is low byte
  // 5th byte is high byte
  strength = (frame[5] << 8) + frame[4];
}

// Private: Handles the low-level bits of communicating with the TFMini, and detecting some communication errors.
std::vector<uint8_t> TFMini::grabFrame() {
  const uint8_t frameSize = 9;
  std::vector<uint8_t> headerPattern{0x59, 0x59};
  std::vector<uint8_t> buffer(frameSize);

  // Init variables needed to grab a new frame
  int tries = 0;
  int readBytes = 0;
  int increment = 2;
  int headerIndex = 0;
  int max = 30;

  // First, continue to read from port as long as we dont have the header pattern inside the buffer
  while(1){
    // Check that buffer is large enough to contain *increment* more bytes
    if(buffer.size() <= readBytes + increment)
      buffer.resize(readBytes + increment);

    // Read bytes into buffer
    int ret = read(port, buffer.data() + readBytes, increment);
    if( ret == -1){
      state = ERROR_SERIAL_READ;
      return {};
    }
    readBytes += ret;
    // Search for header pattern
    std::vector<uint8_t>::iterator frameStart = std::search(buffer.begin(), buffer.begin() + readBytes, headerPattern.begin(), headerPattern.end());
    if(frameStart != buffer.begin() + readBytes){
      headerIndex = std::distance(buffer.begin(), frameStart);
      break;
    }

    // Increment number of tries and return if there has been to many
    ++tries;
    if( tries >= max){
      state = ERROR_SERIAL_NOHEADER;
      return {};
    }
  }

  // Now that we have found the header pattern, get the rest of the frame if needed
  int neededSize = headerIndex + frameSize;
  int missingBytes = neededSize - readBytes;
  if(missingBytes > 0){
    if(buffer.size() <= neededSize)
      buffer.resize(neededSize);
  
    tries = 0;
    while(1){
      int ret = read(port, buffer.data() + readBytes, neededSize - readBytes);
      if( ret == -1){
        state = ERROR_SERIAL_READ;
        return {};
      }
      readBytes += ret;
  
      if(readBytes == neededSize)
        break;
  
      ++tries;
      if( tries >= max){
        state = ERROR_SERIAL_READ;
        return {};
      }
    }
  }

  // Copy the part of the buffer that contains the frame into a new array and return it
  std::vector<uint8_t> frame(buffer.begin() + headerIndex, buffer.begin() + headerIndex + frameSize);
  state = MEASUREMENT_OK;
  return frame;
}
