#include <TOFSensor.hpp>
#include <wiringSerial.h>
#include <stdexcept>

TOFSensor::TOFSensor(std::string device_name, int baudrate) {
  port = serialOpen(device_name.c_str(), baudrate);
  if( port == -1)
    throw std::runtime_error{"Failed to open port " + device_name};
}

TOFSensor::~TOFSensor()
{
	serialClose(port);
}


