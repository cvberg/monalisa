#ifndef TOFSENSOR_HPP
#define TOFSENSOR_HPP

#include <string>
// Base class for TimeOfFlight sensors that uses a UART connection

class TOFSensor {
  public: 
    TOFSensor(std::string device_name, int baudrate);
    virtual ~TOFSensor();
    
    virtual void takeNewMeasurement() = 0;
    virtual uint16_t getDistance() = 0;
    virtual uint16_t getStrength() = 0;
  protected:
    int port;
};


#endif //TOFSENSOR_HPP
