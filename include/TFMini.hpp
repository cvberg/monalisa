#ifndef TFMINI_HPP
#define TFMINI_HPP 

#include <TOFSensor.hpp>
#include <string>
#include <vector>

class TFMini : public TOFSensor {
  public: 
    TFMini(std::string device_name);
    // Configuration
    void setSingleScanMode();
    
    // Data collection
    uint16_t getDistance();
    uint16_t getStrength();
    void takeNewMeasurement();
  private:
    int state;
    uint16_t distance;
    uint16_t strength;
    
    // Low-level communication
    void setStandardOutputMode();
    std::vector<uint8_t> grabFrame();
};


#endif //TFMINI_HPP
